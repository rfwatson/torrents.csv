#!/bin/sh

# This fetches from several torrent websites for new updates
cd ../new_torrents_fetcher
sort -r --field-separator=';' -n --key=5 ../torrents.csv > ../torrents.csv.seeders.desc
cargo run --release -- -s "$1" -f -t ../torrents.csv.seeders.desc
rm ../torrents.csv.seeders.desc
cd ../scripts
./scan_torrents.sh "$1"
