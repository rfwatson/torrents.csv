# Optionally use environment variables

# export TORRENTS_CSV_ENDPOINT=http://0.0.0.0:8080
# export TORRENTS_CSV_DB_FILE=`pwd`/../../torrents.db
# export TORRENTS_CSV_FRONT_END_DIR=`pwd`/../ui/dist

# Build the sqlite db from the CSV
./build_sqlite.sh -f

# Build front end
cd ../server/ui
yarn
yarn build

# Build back end
cd ../service
cargo run --release
